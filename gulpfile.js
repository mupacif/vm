const gulp = require('gulp');
const browserSync = require('browser-sync').create();


//watch sass & server
gulp.task('serve', function()
{
	browserSync.init({server:"."});

	gulp.watch(['*.html','*.css']).on('change',browserSync.reload);
});


gulp.task('default', ['serve']);